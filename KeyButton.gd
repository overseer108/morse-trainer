extends Button

onready var key_shortcut: ShortCut = shortcut

func _ready():
	pass


func _on_WPMEdit_focus_entered():
	shortcut = null


func _on_WPMEdit_focus_exited():
	shortcut = key_shortcut
