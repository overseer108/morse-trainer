extends Reference
class_name SaveState

export(int) var wpm = 12 setget set_wpm
export(bool) var display_reference = true setget set_display_reference

var config_file = ConfigFile.new()
const CONFIG_PATH = "user://settings.ini"
const SETTINGS_SECTION = "settings"
const WPM_KEY = "wpm"
const DISPLAY_MORSE_HELP_KEY = "display_morse_help"

func _init():
	var err = config_file.load(CONFIG_PATH)
	if err == OK:
		wpm = config_file.get_value(SETTINGS_SECTION, WPM_KEY, wpm)
		display_reference = config_file.get_value(SETTINGS_SECTION, DISPLAY_MORSE_HELP_KEY, display_reference)
	else:
		config_file.set_value(SETTINGS_SECTION, WPM_KEY, wpm)
		config_file.set_value(SETTINGS_SECTION, DISPLAY_MORSE_HELP_KEY, display_reference)
		save()

func set_wpm(new_wpm: int):
	wpm = new_wpm
	config_file.set_value(SETTINGS_SECTION, WPM_KEY, wpm)
	save()

func set_display_reference(new_display_ref: bool):
	display_reference = new_display_ref
	config_file.set_value(SETTINGS_SECTION, DISPLAY_MORSE_HELP_KEY, display_reference)
	save()

func save():
	config_file.save(CONFIG_PATH)
