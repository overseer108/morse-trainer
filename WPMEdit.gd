extends LineEdit

signal wpm_changed(new_wpm)

func _ready():
	pass


func _on_WPMEdit_text_changed(new_text):
	var new_wpm = int(new_text)
	if new_wpm > 0:
		emit_signal("wpm_changed", new_wpm)
		add_color_override("font_color", Color.white)
	else:
		add_color_override("font_color", Color.crimson)

func _gui_input(event):
	if event.is_action_pressed("ui_accept") or event.is_action_pressed("ui_cancel"):
		release_focus()
