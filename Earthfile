FROM barichello/godot-ci:3.3.2

WORKDIR /home/morse
COPY . .
RUN mkdir -pv target/www target/bin

webserver:
	FROM nginx:stable-alpine
	COPY +web/www/* /usr/share/nginx/html
	SAVE IMAGE morse-server

web:
	RUN godot --export HTML5 target/www/index.html
	SAVE ARTIFACT target/www /www AS LOCAL target/www

deploy:
	FROM amazon/aws-cli
	COPY "+windows-compressed/Morse Playground Windows.exe.gz" "Morse Playground Windows.exe.gz"
	COPY "+linux-compressed/Morse Playground.x86_64.gz" "Morse Playground.x86_64.gz"
	COPY "+mac/Morse Playground Mac.zip" "Morse Playground Mac.zip"
	# Push compressed executable to public s3 bucket, set access control to public-read and mark it as gzipped
	RUN --push \
		--secret AWS_ACCESS_KEY_ID=+secrets/AWS_ACCESS_KEY_ID \
		--secret AWS_SECRET_ACCESS_KEY=+secrets/AWS_SECRET_ACCESS_KEY \
		aws s3 cp "Morse Playground.x86_64.gz" s3://locloud-public/morse-trainer.x86_64 --acl public-read --content-encoding gzip; \
		aws s3 cp "Morse Playground Windows.exe.gz" s3://locloud-public/morse-trainer.exe --acl public-read --content-encoding gzip; \
		aws s3 cp "Morse Playground Mac.zip" s3://locloud-public/morse-trainer.zip --acl public-read;

archives:
	BUILD +windows-compressed
	BUILD +linux-compressed
	BUILD +mac

windows-compressed:
	FROM +windows
	RUN gzip -9 "target/bin/Morse Playground.exe" 
	SAVE ARTIFACT "target/bin/Morse Playground.exe.gz" "/Morse Playground Windows.exe.gz"

linux-compressed:
	FROM +linux
	RUN gzip -9 "target/bin/Morse Playground.x86_64"
	SAVE ARTIFACT "target/bin/Morse Playground.x86_64.gz" "/Morse Playground.x86_64.gz"

binaries:
	BUILD +windows
	BUILD +linux
	BUILD +mac

windows:
	RUN	godot --export "Windows Desktop" "target/bin/Morse Playground.exe"
	SAVE ARTIFACT "target/bin/Morse Playground.exe" "/Morse Playground.exe" AS LOCAL "target/bin/Morse Playground.exe"

linux:
	RUN	godot --export "Linux/X11" "target/bin/Morse Playground.x86_64"
	SAVE ARTIFACT "target/bin/Morse Playground.x86_64" "/Morse Playground.x86_64" AS LOCAL "target/bin/Morse Playground.x86_64"

mac:
	RUN	godot --export "Mac OSX" "target/bin/Morse Playground Mac.zip"
	SAVE ARTIFACT "target/bin/Morse Playground Mac.zip" "/Morse Playground Mac.zip" AS LOCAL "target/bin/Morse Playground Mac.zip"
