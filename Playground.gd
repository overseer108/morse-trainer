extends Node2D
class_name MorsePlayground

var save_state = SaveState.new()

onready var audio = $AudioStreamPlayer
onready var display = $CanvasLayer/PanelContainer/HBoxContainer/VBoxContainer/PanelContainer/MarginContainer/ScrollContainer/RichTextLabel
onready var GUI_panel = $CanvasLayer/PanelContainer
 
export(Resource) var morse_alphabet

onready var morse_transliterator = MorseTransliterator.new(save_state.wpm, self)

func _ready():
	init_state(save_state)
	morse_transliterator.connect("morse_character_completed", self, "_on_morse_char_completed")

func init_state(save_data: SaveState):
	var wpmText = $CanvasLayer/PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/HBoxContainer/WPMEdit
	wpmText.text = "%d" % save_data.wpm
	
	var reference_button = $CanvasLayer/PanelContainer/HBoxContainer/VBoxContainer/HBoxContainer/HBoxContainer2/ShowReference
	reference_button.pressed = save_data.display_reference
	update_reference_visibility(save_data.display_reference)

func _on_key_button_down():
	audio.play_morse()
	morse_transliterator.key_down()

func _on_key_button_up():
	audio.pause()
	morse_transliterator.key_up()
	
## UI FUNCTIONS ##
var incomplete_morse = ""
func _on_morse_char_completed(morse_char):
	if morse_char == " ":
		var lookup_char = morse_alphabet.alphabet.get(incomplete_morse)
		if lookup_char:
			display.append_msg(lookup_char)
		else:
			display.append_msg(incomplete_morse)
		incomplete_morse = ""
		display.update_morse("")
	elif morse_char == "/":
		display.append_msg(" ")
	elif morse_char == "\n":
		display.append_msg("\n")
	else:
		incomplete_morse += morse_char
		display.update_morse(incomplete_morse)
	pass

func _on_ShowReference_toggled(button_pressed):
	save_state.set_display_reference(button_pressed)
	update_reference_visibility(button_pressed)
	pass

func update_reference_visibility(visible):
	$CanvasLayer/PanelContainer/HBoxContainer/MarginTooltip/ReferencePanel.visible = visible
	GUI_panel._on_window_resized()

func _on_WPMEdit_wpm_changed(new_wpm):
	save_state.wpm = new_wpm
	morse_transliterator.set_wpm(new_wpm)

func _on_ClearButton_pressed():
	incomplete_morse = ""
	display.clear()
	morse_transliterator.reset()
