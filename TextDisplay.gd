extends RichTextLabel

var contents = ""
var morse=""

onready var scroll_container: ScrollContainer = get_node("..")

func _ready():
	refresh()

func append_msg(string):
	contents=contents+string
	refresh()

func update_morse(new_morse):
	morse = new_morse
	refresh()

func refresh():
	bbcode_text = "[center]%s %s[/center]" % [contents, morse]
	
	# Wait for scroll to update with new contents
	yield(get_tree().create_timer(0.1), "timeout") 
	scroll_container.scroll_vertical += 100

func clear():
	contents=""
	morse=""
	refresh()
