class MorseState:
	signal morse_character_completed(morse_char)
	signal state_transition(new_state_class)

	# Virtual methods
	func key_up():
		assert(false, "Please override `key_released` in the derived script.")
		pass
		
	func key_down():
		assert(false, "Please override `key_pressed` in the derived script.")
		pass
		
	func _timer_timeout():
		assert(false, "Please override `_timer_timeout` in the derived script.")
		pass

	# Override and return more than 0 to have the timer trigger
	func _get_timer_duration_dits():
		return 0

	# protected utility methods
	func _morse_char_complete(morse_char: String):
		emit_signal("morse_character_completed", morse_char)

	func _transition_state(new_class):
		emit_signal("state_transition", new_class)
		pass	

class InitialState extends MorseState:

	func key_up():
		print("Key was released in initial state, odd")
		pass
		
	func key_down():
		_transition_state(MorseTransliterator.State.MID_MORSE_CHAR)
		pass
		
	func _timer_timeout():
		assert(false, "Timer cannot timeout in initial state")
		pass

class MidMorseChar extends MorseState:
	var dah = false
	
	func key_up():
		if dah:
			_morse_char_complete("-")
		else:
			_morse_char_complete(".")
		
		dah = false
		_transition_state(MorseTransliterator.State.EXPECTING_MORSE_CHAR_END)
		
	func key_down():
		print("Key was pressed MidMorseChar, odd")
		dah = false
		_transition_state(MorseTransliterator.State.INITIAL)
		
	func _get_timer_duration_dits():
		return 2
		
	func _timer_timeout():
		dah = true

class ExpectingMorseCharEnd extends MorseState:
	func key_up():
		print("Key was released ExpectingMorseCharEnd, odd")
		pass
		
	func key_down():
		_transition_state(MorseTransliterator.State.MID_MORSE_CHAR)
		pass
		
	func _timer_timeout():
		_morse_char_complete(" ")
		_transition_state(MorseTransliterator.State.EXPECTING_CHAR_END)
		pass
		
	func _get_timer_duration_dits():
		return 2

class ExpectingCharEnd extends MorseState:
	func key_up():
		print("Key was released ExpectingCharEnd, odd")
		pass
		
	func key_down():
		_transition_state(MorseTransliterator.State.MID_MORSE_CHAR)
		pass
		
	func _timer_timeout():
		_morse_char_complete("/")
		_transition_state(MorseTransliterator.State.EXPECTING_MSG_END)
		pass
		
	func _get_timer_duration_dits():
		return 5 # 7 - 2
		
class ExpectingMsgEnd extends MorseState:
	func key_up():
		print("Key was released ExpectingMsgEnd, odd")
		pass
		
	func key_down():
		_transition_state(MorseTransliterator.State.MID_MORSE_CHAR)
		pass
		
	func _timer_timeout():
		_morse_char_complete("\n")
		_transition_state(MorseTransliterator.State.INITIAL)
		pass
		
	func _get_timer_duration_dits():
		return 6 # 10 - 4
