extends Reference
class_name MorseTransliterator

signal morse_character_completed(character)

var current_state

var timer = Timer.new()

var dit_ms

enum State{
	INITIAL, 
	MID_MORSE_CHAR,
	EXPECTING_MORSE_CHAR_END,  
	EXPECTING_CHAR_END, 
	EXPECTING_MSG_END
}

var MORSE_STATES_FILE = load("res://MorseStateMachine/MorseState.gd")
var states = { 
	State.INITIAL: MORSE_STATES_FILE.InitialState.new(),
	State.MID_MORSE_CHAR: MORSE_STATES_FILE.MidMorseChar.new(),
	State.EXPECTING_MORSE_CHAR_END: MORSE_STATES_FILE.ExpectingMorseCharEnd.new(),
	State.EXPECTING_CHAR_END: MORSE_STATES_FILE.ExpectingCharEnd.new(),
	State.EXPECTING_MSG_END: MORSE_STATES_FILE.ExpectingMsgEnd.new()
}

func _init(wpm: int, node: Node):
	node.add_child(timer)
	timer.one_shot = true
	set_wpm(wpm)
	
func key_down():
	current_state.key_down()
	pass
	
func key_up():
	current_state.key_up()
	pass
	
func reset():
	transition_state(State.INITIAL)
	pass

func transition_state(new_state):
	timer.stop()
	if current_state != null:
		current_state.disconnect("state_transition", self, "transition_state")
		current_state.disconnect("morse_character_completed", self, "_on_morse_character_completed")
		timer.disconnect("timeout", current_state, "_timer_timeout")
	current_state = states[new_state]
	current_state.connect("state_transition", self, "transition_state")
	current_state.connect("morse_character_completed", self, "_on_morse_character_completed")
	var requested_timer =  current_state._get_timer_duration_dits()
	if requested_timer > 0:
		timer.connect("timeout", current_state, "_timer_timeout")
		timer.start(0.001 * requested_timer * dit_ms)

func set_wpm(wpm):
	dit_ms = _wpm_to_dit_ms(wpm)
	reset()
	
func _on_morse_character_completed(new_char: String):
	emit_signal("morse_character_completed", new_char)

static func _wpm_to_dit_ms(wpm: int) -> int:
	return 1200/wpm
