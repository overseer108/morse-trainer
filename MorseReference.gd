extends Node

export(Resource) var morse_alphabet
onready var left = $Left
onready var right = $Right

func _ready():
	var alphabet: Dictionary = morse_alphabet.alphabet
	var sortedLetters = []
	
	for morse in alphabet.keys():
		var kv = [morse, alphabet[morse]]
		sortedLetters.push_back(kv)
		
	sortedLetters.sort_custom(MorseSorter, "sort")
	
	for letter in sortedLetters:
		left.text += "%s:\n" % letter[1]
		right.text += "%s\n" % letter[0]
		
	left.text = left.text.substr(0,left.text.length()-1)
	right.text = right.text.substr(0,right.text.length()-1)
		
class MorseSorter:
	static func sort(a, b):
		var a_is_alphabetic = a[1] >= "A"
		var b_is_alphabetic = b[1] >= "A"
		
		if a_is_alphabetic == b_is_alphabetic:
			return a[1] < b[1]
		if a_is_alphabetic:
			return true
		if b_is_alphabetic:
			return false
