extends PanelContainer

func _ready():
	get_tree().get_root().connect("size_changed", self, "_on_window_resized")
	_on_window_resized()

func _on_window_resized():
	rect_size = get_viewport_rect().size
